package phantom.robots.adina

import phantom.robots.adina.events.emitters.GearboxChangedEventEmitter
import phantom.robots.adina.events.emitters.HeadingUpdatedEventEmitter
import phantom.robots.adina.gamepad.GamepadMaps
import phantom.robots.adina.hardware.HardwareMapConfigurations

object Adina {
    public val gamepadMap = GamepadMaps.default
    public val hardwareConfiguration = HardwareMapConfigurations.default

    public object events {
        public val headingUpdated = HeadingUpdatedEventEmitter()
        public val gearboxChanged = GearboxChangedEventEmitter()
    }
}
package phantom.robots.adina.utils

object CircleMath {
    public enum class TurnDirection {
        LEFT,
        RIGHT
    }

    public enum class MoveStates {
        MOVE,
        STRAFE,
        ROTATE
    }

    public fun getTurnDirection(current: Int, target: Int): TurnDirection {
        return if (current >= target)
            if (Math.abs(current - target) > 180) TurnDirection.LEFT else TurnDirection.RIGHT
        else
            if (Math.abs(current - target) > 180) TurnDirection.RIGHT else TurnDirection.LEFT
    }

    private fun opposite(angle: Int): Int {
        return (angle + 180) % 360;
    }
}
package phantom.robots.adina.hardware

object HardwareMapConfigurations {
    public val default = DefaultHardwareMapConfiguration()
    public val demo = DemoHardwareMapConfiguration()
}
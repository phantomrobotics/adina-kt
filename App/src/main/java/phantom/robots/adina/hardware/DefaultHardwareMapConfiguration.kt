package phantom.robots.adina.hardware

class DefaultHardwareMapConfiguration: HardwareMapConfiguration() {
    override fun of(hub: HardwareHub): String {
        return when(hub) {
            HardwareHub.SENSORS -> "hubs.sensors"

            HardwareHub.FRONT_DRIVE -> "hubs.motors.front_drive"
            HardwareHub.BACK_DRIVE -> "hubs.motors.back_drive"
        }
    }

    override fun of(component: HardwareComponents): String {
        return when(component) {
            HardwareComponents.GYRO -> "sensors.orientation.gyro"
            HardwareComponents.DISTANCE_SENSOR -> "sensors.distance"

            HardwareComponents.FRONT_LEFT_MOTOR -> "motors.drive.front.left"
            HardwareComponents.FRONT_RIGHT_MOTOR -> "motors.drive.front.right"
            HardwareComponents.BACK_LEFT_MOTOR -> "motors.drive.back.left"
            HardwareComponents.BACK_RIGHT_MOTOR -> "motors.drive.back.right"
        }
    }
}
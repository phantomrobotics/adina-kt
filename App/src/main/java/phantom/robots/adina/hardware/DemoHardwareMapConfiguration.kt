package phantom.robots.adina.hardware

class DemoHardwareMapConfiguration: HardwareMapConfiguration() {
    override fun of(hub: HardwareHub): String {
        return when(hub) {
            HardwareHub.SENSORS -> "Sensors"

            HardwareHub.FRONT_DRIVE -> "FrontMotors"
            HardwareHub.BACK_DRIVE -> "BackMotors"
        }
    }

    override fun of(component: HardwareComponents): String {
        return when(component) {
            HardwareComponents.GYRO -> "gyro"
            HardwareComponents.DISTANCE_SENSOR -> "distance"

            HardwareComponents.FRONT_LEFT_MOTOR -> "frontLeft"
            HardwareComponents.FRONT_RIGHT_MOTOR -> "frontRight"
            HardwareComponents.BACK_LEFT_MOTOR -> "backLeft"
            HardwareComponents.BACK_RIGHT_MOTOR -> "backRight"
        }
    }
}
package phantom.robots.adina.hardware

enum class HardwareHub {
    SENSORS,

    FRONT_DRIVE,
    BACK_DRIVE
}

enum class HardwareComponents {
    GYRO,
    DISTANCE_SENSOR,

    FRONT_LEFT_MOTOR,
    FRONT_RIGHT_MOTOR,
    BACK_LEFT_MOTOR,
    BACK_RIGHT_MOTOR
}

abstract class HardwareMapConfiguration {
    public abstract fun of(hub: HardwareHub): String;
    public abstract fun of(component: HardwareComponents): String;
}
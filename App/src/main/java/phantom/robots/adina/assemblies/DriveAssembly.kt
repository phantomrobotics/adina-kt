package phantom.robots.adina.assemblies

import com.qualcomm.robotcore.hardware.DcMotor
import com.qualcomm.robotcore.hardware.DcMotorSimple
import labs.phantom.sdk.events.by
import labs.phantom.sdk.events.using
import labs.phantom.sdk.hardware.export
import labs.phantom.sdk.logic.assembly.GameAssembly
import phantom.robots.adina.Adina
import phantom.robots.adina.events.GearboxChangedDirection
import phantom.robots.adina.hardware.HardwareComponents
import phantom.robots.adina.utils.CircleMath
import android.provider.SyncStateContract.Helpers.update

object DriveAssembly : GameAssembly() {
    private object Motors {
        object front {
            public val left = 0
            public val right = 1
        }

        object back {
            public val left = 2
            public val right = 3
        }
    }

    private lateinit var motors: List<DcMotor>;

    private var locked: Boolean = false
        set(value) {
            if(value) this.stop()
            field = value
        }

    private var speed: Double = 1.0
    private val gearSpeeds: List<Double> = listOf(0.25, 0.5, 0.75, 1.0)
    private var gear: Int = 3
        set(value) {
            if(value >= 0 && value < this.gearSpeeds.size) {
                this.speed = gearSpeeds[value]
                field = value
            }
        }

    private var heading: Int = 0;

    init {
        this.configure {
            it.initialized {
                this.motors = listOf(
                        hardware export Adina.hardwareConfiguration.of(HardwareComponents.FRONT_LEFT_MOTOR),
                        hardware export Adina.hardwareConfiguration.of(HardwareComponents.FRONT_RIGHT_MOTOR),

                        hardware export Adina.hardwareConfiguration.of(HardwareComponents.BACK_LEFT_MOTOR),
                        hardware export Adina.hardwareConfiguration.of(HardwareComponents.BACK_RIGHT_MOTOR)
                );

                for (motor in this.motors) {
                    motor.mode = DcMotor.RunMode.STOP_AND_RESET_ENCODER;
                    Thread.sleep(25);
                    motor.mode = DcMotor.RunMode.RUN_USING_ENCODER;
                }

                this.motors[Motors.front.left].direction = DcMotorSimple.Direction.REVERSE;
                this.motors[Motors.front.right].direction = DcMotorSimple.Direction.REVERSE;
            }

            watch by Adina.events.headingUpdated using {
                this.heading = it.heading;
            }
        }
    }

    private fun moveToTarget(state: CircleMath.MoveStates, target: Int, speed: Double) {
        if(state != CircleMath.MoveStates.ROTATE) {
            for (motor in motors) {
                motor.mode = DcMotor.RunMode.STOP_AND_RESET_ENCODER
                Thread.sleep(20)
            }
        }

        when(state) {
            CircleMath.MoveStates.ROTATE -> this.turnToTarget(target, speed)

            CircleMath.MoveStates.MOVE -> {
                for (motor in motors) {
                    motor.mode = DcMotor.RunMode.RUN_TO_POSITION
                    motor.targetPosition = target
                    motor.power = speed
                }
            }

            CircleMath.MoveStates.STRAFE -> {
                motors[Motors.front.left].targetPosition = target
                motors[Motors.front.right].targetPosition = -target
                motors[Motors.back.left].targetPosition = -target
                motors[Motors.back.right].targetPosition = target

                for (motor in motors) {
                    motor.mode = DcMotor.RunMode.RUN_TO_POSITION
                    motor.power = speed
                }
            }
        }

        if(state != CircleMath.MoveStates.ROTATE) {
            while (motors[0].isBusy && unit.opModeIsActive());

            for (motor in motors) {
                motor.power = 0.0
                motor.mode = DcMotor.RunMode.RUN_USING_ENCODER
            }
        }
    }

    private fun turnToTarget(target: Int, power: Double) {
        if (CircleMath.getTurnDirection(this.heading, target) === CircleMath.TurnDirection.RIGHT) {
            set(0.0, 0.0, power)
        } else {
            set(0.0, 0.0, -power)
        }
        val lower = Math.max(target - 3, 0)
        val higher = Math.min(target + 3, 359)

        var heading = GyroAssembly.heading
        while (unit.opModeIsActive()) {
            heading = GyroAssembly.heading
            if (heading >= lower && heading <= higher) break
        }

        set(0.0, 0.0, 0.0)
    }


    public fun set(strafe: Double, direction: Double, rotation: Double, time: Long = 0) {
        if(this.locked) return;

        val r = Math.hypot(strafe, direction);
        val robotAngle = Math.atan2(direction, strafe) - Math.PI / 4;

        this.motors[Motors.front.left].power = this.speed * (r * Math.cos(robotAngle) + rotation);
        this.motors[Motors.front.right].power = this.speed * (r * Math.sin(robotAngle) - rotation);
        this.motors[Motors.back.left].power = this.speed * (r * Math.sin(robotAngle) + rotation);
        this.motors[Motors.back.right].power = this.speed * (r * Math.cos(robotAngle) - rotation);

        if(time > 0) {
            Thread.sleep(time);
            this.stop();
        }
    }


    public fun stop() {
        for(motor in this.motors) {
            motor.power = 0.0;
        }
    }

    public fun changeGear(direction: GearboxChangedDirection) {
        if(direction == GearboxChangedDirection.UP) this.gear++;
        else this.gear--;
    }
}
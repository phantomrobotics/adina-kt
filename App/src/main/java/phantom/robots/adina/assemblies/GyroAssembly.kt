package phantom.robots.adina.assemblies

import com.qualcomm.robotcore.hardware.GyroSensor
import labs.phantom.sdk.hardware.export
import labs.phantom.sdk.hardware.log
import labs.phantom.sdk.logic.assembly.GameAssembly
import phantom.robots.adina.Adina
import phantom.robots.adina.hardware.HardwareComponents

object GyroAssembly : GameAssembly() {
    private lateinit var gyro: GyroSensor;
    public var heading = 0

    init {
        this.configure {
            it.initialized {
                this.gyro = hardware export Adina.hardwareConfiguration.of(HardwareComponents.GYRO)

                unit.telemetry log "Gyro sensor is calibrating!"
                this.gyro.calibrate();

                while (this.gyro.isCalibrating());
                unit.telemetry log "Gyro sensor was calibrated!"
            }

            it.updated {
                this.heading = this.gyro.heading;
            }
        }
    }
}
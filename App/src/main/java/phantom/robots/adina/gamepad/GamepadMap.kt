package phantom.robots.adina.gamepad

import com.qualcomm.robotcore.hardware.Gamepad

enum class VariableControls {
    DIRECTION_AXIS,
    STRAFE_AXIS,
    ROTATION_AXIS
}

enum class StateControls {
    LIFT_ACTION,
    TEST_ACTION,
    GEARBOX_UP_ACTION,
    GEARBOX_DOWN_ACTION
}

abstract class GamepadMap {
    public abstract fun control(pad: Gamepad, type: VariableControls): Double;
    public abstract fun control(pad: Gamepad, type: StateControls): Boolean;
}

package phantom.robots.adina.gamepad

import com.qualcomm.robotcore.hardware.Gamepad

class DefaultGamepadMap: GamepadMap() {
    override fun control(pad: Gamepad, type: VariableControls): Double {
        return when(type) {
            VariableControls.DIRECTION_AXIS -> pad.left_stick_x.toDouble()
            VariableControls.STRAFE_AXIS -> -pad.left_stick_y.toDouble()
            VariableControls.ROTATION_AXIS -> -pad.right_stick_y.toDouble()
        }
    }

    override fun control(pad: Gamepad, type: StateControls): Boolean {
        return when(type) {
            StateControls.LIFT_ACTION -> pad.a
            StateControls.TEST_ACTION -> pad.b
            StateControls.GEARBOX_UP_ACTION -> pad.right_bumper
            StateControls.GEARBOX_DOWN_ACTION -> pad.left_bumper
        }
    }
}
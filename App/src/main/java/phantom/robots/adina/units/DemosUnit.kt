package phantom.robots.adina.units

import com.qualcomm.robotcore.eventloop.opmode.TeleOp
import labs.phantom.sdk.hardware.gamepad.GamepadModule
import labs.phantom.sdk.logic.GameUnit
import labs.phantom.sdk.logic.module.load
import phantom.robots.adina.modules.DriveModule
import phantom.robots.adina.modules.GyroModule
import phantom.robots.adina.modules.ManualDriveModule
import phantom.robots.adina.modules.ManualGearboxModule

@TeleOp(name = "DemosUnit")
class DemosUnit : GameUnit() {
    init {
        modules load GamepadModule()

        modules load GyroModule()

        modules load DriveModule()
        modules load ManualDriveModule()

        modules load ManualGearboxModule()
    }
}


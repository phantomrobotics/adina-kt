package phantom.robots.adina.events

import labs.phantom.sdk.events.GameEvent

enum class GearboxChangedDirection {
    UP,
    DOWN
}

class GearboxChangedEvent(public val direction: GearboxChangedDirection): GameEvent() { }
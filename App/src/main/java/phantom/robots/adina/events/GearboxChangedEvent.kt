package phantom.robots.adina.events

import labs.phantom.sdk.events.GameEvent

class HeadingUpdatedEvent(public val heading: Int): GameEvent() { }
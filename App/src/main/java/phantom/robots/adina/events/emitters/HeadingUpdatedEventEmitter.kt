package phantom.robots.adina.events.emitters

import labs.phantom.sdk.events.GameEventEmitter
import phantom.robots.adina.events.HeadingUpdatedEvent

class HeadingUpdatedEventEmitter: GameEventEmitter<HeadingUpdatedEvent>() { }
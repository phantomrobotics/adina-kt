package phantom.robots.adina.modules

import labs.phantom.sdk.logic.assembly.load
import labs.phantom.sdk.logic.module.GameModule
import phantom.robots.adina.assemblies.DriveAssembly

class DriveModule : GameModule() {
    init {
        assemblies load DriveAssembly

        this.configure {
            it.stopped {
                DriveAssembly.stop()
            }
        }
    }
}
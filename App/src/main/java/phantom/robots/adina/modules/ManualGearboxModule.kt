package phantom.robots.adina.modules

import labs.phantom.sdk.events.by
import labs.phantom.sdk.events.using
import labs.phantom.sdk.hardware.gamepad.GAMEPAD
import labs.phantom.sdk.hardware.gamepad.Gamepads
import labs.phantom.sdk.hardware.gamepad.sourced
import labs.phantom.sdk.logic.module.GameModule
import phantom.robots.adina.Adina
import phantom.robots.adina.events.GearboxChangedDirection
import phantom.robots.adina.events.GearboxChangedEvent
import phantom.robots.adina.gamepad.StateControls

class ManualGearboxModule : GameModule() {
    init {
        this.configure {
            watch by GAMEPAD.events using {
                if(Gamepads.FIRST sourced it) {
                    if(Adina.gamepadMap.control(it.pad, StateControls.GEARBOX_UP_ACTION)) {
                        Adina.events.gearboxChanged.trigger(GearboxChangedEvent(GearboxChangedDirection.UP));
                    } else if(Adina.gamepadMap.control(it.pad, StateControls.GEARBOX_DOWN_ACTION)) {
                        Adina.events.gearboxChanged.trigger(GearboxChangedEvent(GearboxChangedDirection.DOWN));
                    }
                }
            }
        }
    }
}
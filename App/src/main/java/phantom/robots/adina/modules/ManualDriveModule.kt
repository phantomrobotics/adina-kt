package phantom.robots.adina.modules

import labs.phantom.sdk.events.by
import labs.phantom.sdk.events.using
import labs.phantom.sdk.hardware.gamepad.GAMEPAD
import labs.phantom.sdk.hardware.gamepad.Gamepads
import labs.phantom.sdk.hardware.gamepad.sourced
import labs.phantom.sdk.hardware.log
import labs.phantom.sdk.logic.assembly.load
import labs.phantom.sdk.logic.module.GameModule
import labs.phantom.sdk.logic.module.load
import phantom.robots.adina.Adina
import phantom.robots.adina.assemblies.DriveAssembly
import phantom.robots.adina.gamepad.StateControls
import phantom.robots.adina.gamepad.VariableControls

class ManualDriveModule : GameModule() {

    init {
        this.configure {
            watch by GAMEPAD.events using {
                if(Gamepads.FIRST sourced it) {
                    if(Adina.gamepadMap.control(it.pad, StateControls.TEST_ACTION)) {
                        unit.telemetry log "Test action has been pressed!"
                    }

                    DriveAssembly.set(
                            Adina.gamepadMap.control(it.pad, VariableControls.STRAFE_AXIS),
                            Adina.gamepadMap.control(it.pad, VariableControls.DIRECTION_AXIS),
                            Adina.gamepadMap.control(it.pad, VariableControls.ROTATION_AXIS)
                    );
                }

            }
        }
    }
}
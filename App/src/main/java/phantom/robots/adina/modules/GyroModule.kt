package phantom.robots.adina.modules

import labs.phantom.sdk.logic.assembly.load
import labs.phantom.sdk.logic.module.GameModule
import phantom.robots.adina.Adina
import phantom.robots.adina.assemblies.GyroAssembly
import phantom.robots.adina.events.HeadingUpdatedEvent

class GyroModule : GameModule() {
    init {
        assemblies load GyroAssembly

        this.configure {
            it.updated {
                Adina.events.headingUpdated.trigger(HeadingUpdatedEvent(GyroAssembly.heading));
            }
        }
    }
}
package labs.phantom.sdk.logic.assembly

import com.qualcomm.robotcore.hardware.HardwareMap
import labs.phantom.sdk.events.Watch
import labs.phantom.sdk.logic.GameUnit
import labs.phantom.sdk.logic.module.GameModule
import labs.phantom.sdk.logic.module.GameModuleTriggers
import labs.phantom.sdk.tooling.ParameterizedCallback
import labs.phantom.sdk.tooling.VoidCallback
import java.lang.Exception

abstract class GameAssembly(public val updateable: Boolean = true) {
    private var configured = false
    private var initialized = false
    private var started = false
    private var updated = -1
    private var stopped = false
    private var loaded = false

    private var configureCallback: ParameterizedCallback<GameAssembly> = {}
    private var initializedCallback: VoidCallback = { }
    private var startedCallback: VoidCallback = { }
    private var updatedCallback: VoidCallback = { }
    private var stoppedCallback: VoidCallback = { }

    public fun configure(configureCallback: ParameterizedCallback<GameAssembly>) {
        this.configureCallback = configureCallback
    }

    public fun initialized(initializedCallback: VoidCallback) {
        this.initializedCallback = initializedCallback
    }

    public fun started(startedCallback: VoidCallback) {
        this.startedCallback = startedCallback
    }

    public fun updated(updatedCallback: VoidCallback) {
        this.updatedCallback = updatedCallback
    }

    public fun stopped(stoppedCallback: VoidCallback) {
        this.stoppedCallback = stoppedCallback
    }

    public lateinit var unit: GameUnit
    public lateinit var module: GameModule
    public lateinit var watch: Watch
    public lateinit var hardware: HardwareMap

    public fun install(unit: GameUnit, module: GameModule, watch: Watch, hardware: HardwareMap) {
        if(this.loaded) throw Exception("The assembly was already loaded!")

        this.loaded = true
        this.unit = unit
        this.module = module
        this.watch = watch
        this.hardware = hardware
    }

    public fun launch(what: GameModuleTriggers, updateTick: Int) {
        when(what) {
            GameModuleTriggers.CONFIGURE -> {
                if(!this.configured) {
                    configureCallback(this)
                    this.configured = true
                }
            }

            GameModuleTriggers.INIT -> {
                if(!this.initialized) {
                    initializedCallback()
                    this.initialized = true
                }
            }

            GameModuleTriggers.START -> {
                if(!this.started) {
                    startedCallback()
                    this.started = false
                }
            }

            GameModuleTriggers.UPDATE -> {
                if(this.updateable && updateTick != this.updated) {
                    updatedCallback()
                    this.updated = updateTick
                }
            }
            GameModuleTriggers.STOP -> {
                if(!this.stopped) {
                    stoppedCallback()
                    this.stopped = true
                }

            }
        }
    }
}
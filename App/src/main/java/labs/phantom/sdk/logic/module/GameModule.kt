package labs.phantom.sdk.logic.module

import com.qualcomm.robotcore.hardware.HardwareMap
import labs.phantom.sdk.events.Watch
import labs.phantom.sdk.logic.GameUnit
import labs.phantom.sdk.logic.assembly.AssemblyLoader
import labs.phantom.sdk.logic.assembly.GameAssembly
import labs.phantom.sdk.tooling.ParameterizedCallback
import labs.phantom.sdk.tooling.VoidCallback
import java.lang.Exception

abstract class GameModule(public val updateable: Boolean = true) {
    private var configured = false
    private var initialized = false
    private var started = false
    private var updated = -1
    private var stopped = false
    private var loaded = false

    private var configureCallback: ParameterizedCallback<GameModule> = { }
    private var initializedCallback: VoidCallback = { }
    private var startedCallback: VoidCallback = { }
    private var updatedCallback: VoidCallback = { }
    private var stoppedCallback: VoidCallback = { }

    public fun configure(configureCallback: ParameterizedCallback<GameModule>) {
        this.configureCallback = configureCallback
    }

    public fun initialized(initializedCallback: VoidCallback) {
        this.initializedCallback = initializedCallback
    }

    public fun started(startedCallback: VoidCallback) {
        this.startedCallback = startedCallback
    }

    public fun updated(updatedCallback: VoidCallback) {
        this.updatedCallback = updatedCallback
    }

    public fun stopped(stoppedCallback: VoidCallback) {
        this.stoppedCallback = stoppedCallback
    }

    private val assemblyList: MutableList<GameAssembly> = mutableListOf()
    public fun <Z: GameAssembly> addAssembly(assembly: Z) {
        this.assemblyList.add(assembly)
    }

    public var assemblies: AssemblyLoader

    public lateinit var unit: GameUnit
    public lateinit var watch: Watch
    public lateinit var hardware: HardwareMap

    init {
        this.assemblies = AssemblyLoader(this)
    }

    public fun install(unit: GameUnit, watch: Watch, hardware: HardwareMap) {
        if(this.loaded) throw Exception("The module was already loaded!")

        this.loaded = true
        this.unit = unit
        this.watch = watch
        this.hardware = hardware

        for(assembly in assemblyList) assembly.install(unit, this, watch, hardware)
    }

    public fun launch(what: GameModuleTriggers, updateTick: Int) {
        for(assembly in assemblyList) assembly.launch(what, updateTick)

        when(what) {
            GameModuleTriggers.CONFIGURE -> {
                if(!this.configured) {
                    configureCallback(this)
                    this.configured = true
                }
            }

            GameModuleTriggers.INIT -> {
                if(!this.initialized) {
                    initializedCallback()
                    this.initialized = true
                }
            }

            GameModuleTriggers.START -> {
                if(!this.started) {
                    startedCallback()
                    this.started = false
                }
            }

            GameModuleTriggers.UPDATE -> {
                if(this.updateable && updateTick != this.updated) {
                    updatedCallback()
                    this.updated = updateTick
                }
            }
            GameModuleTriggers.STOP -> {
                if(!this.stopped) {
                    stoppedCallback()
                    this.stopped = true
                }

            }
        }
    }
}
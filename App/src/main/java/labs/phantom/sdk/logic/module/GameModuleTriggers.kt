package labs.phantom.sdk.logic.module

enum class GameModuleTriggers {
    CONFIGURE,
    INIT,
    START,
    UPDATE,
    STOP
}
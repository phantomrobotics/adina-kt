package labs.phantom.sdk.logic

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode
import com.qualcomm.robotcore.hardware.Gamepad
import labs.phantom.sdk.events.Watch
import labs.phantom.sdk.logic.module.GameModule
import labs.phantom.sdk.logic.module.GameModuleTriggers
import labs.phantom.sdk.logic.module.ModuleLoader
import labs.phantom.sdk.tooling.ConfigureCallback
import labs.phantom.sdk.tooling.VoidCallback

abstract class GameUnit(private val updateable: Boolean = true): LinearOpMode() {
    private var updated = 1

    private var configurareCallback: ConfigureCallback = {}

    private var initializedCallback: VoidCallback = {}
    private var startedCallback: VoidCallback = {}
    private var updatedCallback: VoidCallback = {}
    private var stoppedCallback: VoidCallback = {}

    var hardware = hardwareMap

    public fun configure(callback: ConfigureCallback) {
        this.configurareCallback = callback
    }

    public fun initialized(callback: VoidCallback) {
        this.initializedCallback = callback
    }

    public fun started(callback: VoidCallback) {
        this.startedCallback = callback
    }

    public fun updated(callback: VoidCallback) {
        this.updatedCallback = callback
    }

    public fun stopped(callback: VoidCallback) {
        this.stoppedCallback = callback
    }

    public val moduleList: MutableList<GameModule> = mutableListOf()
    public fun addModule(module: GameModule) {
        this.moduleList.add(module)
    }

    public var modules: ModuleLoader
    init {
        this.modules = ModuleLoader(this)
    }

    public val watch = Watch()

    override fun runOpMode() {
        this.hardware = hardwareMap
        for(module in moduleList) module.install(this, watch, hardware)

        for(module in moduleList) module.launch(GameModuleTriggers.CONFIGURE, this.updated)
        this.configurareCallback(this)

        for(module in moduleList) module.launch(GameModuleTriggers.INIT, this.updated)
        this.initializedCallback()

        this.waitForStart()

        for(module in moduleList) module.launch(GameModuleTriggers.START, this.updated)
        this.startedCallback()

        while(this.opModeIsActive() && this.updateable) {
            for(module in moduleList) module.launch(GameModuleTriggers.UPDATE, this.updated)
            this.updated = -this.updated
            this.updatedCallback()
        }

        for(module in moduleList) module.launch(GameModuleTriggers.STOP, this.updated)
        this.stoppedCallback()
    }
}
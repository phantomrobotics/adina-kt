package labs.phantom.sdk.hardware

import org.firstinspires.ftc.robotcore.external.Telemetry

infix fun Telemetry.log(that: String) {
    this.addData("log", that)
    this.update()
}

infix fun Telemetry.error(that: String) {
    this.addData("error", that)
    this.update()
}

infix fun Telemetry.warning(that: String) {
    this.addData("warning", that)
    this.update()
}

 fun Telemetry.push() {
    this.update()
}
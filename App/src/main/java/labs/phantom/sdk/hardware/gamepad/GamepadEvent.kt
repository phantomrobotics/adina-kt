package labs.phantom.sdk.hardware.gamepad

import com.qualcomm.robotcore.hardware.Gamepad
import labs.phantom.sdk.events.GameEvent

class GamepadEvent(public val pad: Gamepad, public val source: Gamepads): GameEvent() { }